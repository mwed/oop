class Article < ApplicationRecord
  has_many :articles_categories
  has_many :categories, through: :articles_categories

  validates :title, presence: true
  validates :body, presence: true

  def form_params
    {
      title: title,
      body: body,
      big_category: category_ids.include?(Category::BIG_ID) ? 1 : 0,
      middle_category: category_ids.include?(Category::MIDDLE_ID) ? 1 : 0,
      small_category: category_ids.include?(Category::SMALL_ID) ? 1 : 0
    }
  end
end
