class ArticleLengthValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    validator = Validator.new(record, value, options)
    unless validator.valid?
      record.errors.add attribute.to_sym, "件名と本文で#{options[:minimum]}以上入力してください"
    end
  end

  class Validator
    def initialize(record, value, options)
      @record = record
      @value = value
      @options = options
    end

    def attribute
      @options[:attribute] || ""
    end

    def minimum
      @options[:minimum] || 0
    end

    def value
      @value || ""
    end

    def valid?
      length > minimum
    end

    def length
      sum = value.length
      sum += @record.send(attribute)&.length unless attribute.blank?
      sum
    end
  end
end
