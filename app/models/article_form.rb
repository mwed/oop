class ArticleForm
  include ActiveModel::Model

  attr_accessor :title, :body, :big_category, :middle_category, :small_category
  attr_reader :article

  delegate :persisted?, to: :article

  validates :title, presence: true, article_length: { attribute: :body, minimum: 10 }
  validates :body, presence: true

  def initialize(article, attributes = {})
    @article = article
    super(attributes)
  end

  def big_category_label
    categories[Category::BIG_ID]
  end

  def middle_category_label
    categories[Category::MIDDLE_ID]
  end

  def small_category_label
    categories[Category::SMALL_ID]
  end

  def create
    return false if invalid?

    article.attributes = params
    CreateArticleAndSendMail.new(article).save
  end

  def update
    return false if invalid?

    article.update(params)
  end

  def params
    { title: title, body: body, category_ids: category_ids }
  end

  def category_ids
    ids = []
    ids << Category::BIG_ID if big_category == "1"
    ids << Category::MIDDLE_ID if middle_category == "1"
    ids << Category::SMALL_ID if small_category == "1"
    ids
  end

  private

    def categories
      @_categories ||= Category.article_checkboxes
    end
end
