class Category < ApplicationRecord
  has_many :articles_categories
  has_many :articles, through: :articles_categories

  validates :name, presence: true, uniqueness: true

  BIG_ID = 1
  MIDDLE_ID = 2
  SMALL_ID = 3

  def self.article_checkboxes
    where(id: [BIG_ID, MIDDLE_ID, SMALL_ID]).inject({}) { |hash, record| hash[record.id] = record.name; hash }
  end
end
