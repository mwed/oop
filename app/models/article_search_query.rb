class ArticleSearchQuery
  include ActiveModel::Model

  attr_writer :keyword
  attr_accessor :from_date, :to_date

  def articles
    scope = Article.all
    scope = scope.where(conditions.join(" AND "), values) if conditions.count.nonzero?
    scope
  end

  def keyword
    "%#{Article.sanitize_sql_like(@keyword)}%" if @keyword
  end

  private

    def conditions
      result = []
      result << "title like :keyword" if keyword
      result << "created_at >= :from_date" if from_date
      result << "created_at <= :to_date" if to_date
      result
    end

    def values
      { from_date: from_date&.to_time, to_date: to_date&.to_time, keyword: keyword }
    end
end
