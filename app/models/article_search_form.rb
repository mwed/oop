class ArticleSearchForm
  include ActiveModel::Model

  attr_accessor :keyword
  attr_writer :from_date, :to_date

  validates :keyword, length: { maximum: 10 }, allow_nil: true
  validate :ensure_to_date_format, :ensure_from_date_format

  def articles
    query.articles
  end

  def from_date
    Date.parse(@from_date)
  rescue
    nil
  end

  def to_date
    Date.parse(@to_date)
  rescue
    nil
  end

  private

    def ensure_to_date_format
      ensure_date_format(:to_date, @to_date)
    end

    def ensure_from_date_format
      ensure_date_format(:from_date, @from_date)
    end

    def ensure_date_format(column, value)
      Date.parse(value) unless value.blank?
    rescue ArgumentError
      errors.add column, "日時の書式にあやまりがあります"
    end

    def query
      ArticleSearchQuery.new(keyword: keyword, from_date: from_date, to_date: to_date)
    end
end
