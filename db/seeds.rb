# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
{1 => "株式", 2 => "投資信託", 3 => "FX"}.each do |key, val|
  if Category.exists?(id: key)
    Category.find(key).update_attributes!(name: val)
  else
    Category.create!(id: key, name: val)
  end
end
