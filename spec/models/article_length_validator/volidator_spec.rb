require "rails_helper"

RSpec.describe ::ArticleLengthValidator::Validator, type: :model do
  describe "#valid?" do
    let(:record) { OpenStruct.new(foo: value) }
    let(:value) { "123456789" }
    let(:validator) { ::ArticleLengthValidator::Validator.new(record, value, options) }

    context "minimum以上の時" do
      let(:options) { { minimum: 10, attribute: :foo } }

      it { expect(validator).to be_valid }
    end

    context "minimum以下の時" do
      let(:options) { { minimum: 100, attribute: :foo } }

      it { expect(validator).not_to be_valid }
    end
  end

  describe "#attribute" do
    let(:validator) { ::ArticleLengthValidator::Validator.new(nil, nil, options) }

    context "attributesの指定がある場合" do
      let(:options) { { attribute: :hoo } }
      it { expect(validator.attribute).to eq :hoo }
    end

    context "attributesの指定がない場合" do
      let(:options) { {} }
      it { expect(validator.attribute).to eq "" }
    end
  end

  describe "#attribute" do
    let(:validator) { ::ArticleLengthValidator::Validator.new(nil, nil, options) }

    context "minimumの指定がある場合" do
      let(:options) { { minimum: 10 } }
      it { expect(validator.minimum).to eq 10 }
    end

    context "attributesの指定がない場合" do
      let(:options) { {} }
      it { expect(validator.minimum).to be_zero }
    end
  end

  describe "#lentgh" do
    let(:record) { OpenStruct.new(foo: value) }
    let(:value) { "123456789" }
    let(:validator) { ::ArticleLengthValidator::Validator.new(record, value, options) }

    context "attributeの設定がある場合" do
      let(:options) { { minimum: 10, attribute: :foo } }

      it { expect(validator.length).to eq 18 }
    end

    context "attributeの設定がない場合" do
      let(:options) { { minimum: 10 } }

      it { expect(validator.length).to eq 9 }
    end
  end
end
