require "rails_helper"

RSpec.describe Article, type: :model do
  describe "#valid?" do
    let(:article) { build(:article) }
    it { expect(article).to be_valid }
  end
end
