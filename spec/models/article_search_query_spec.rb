require "rails_helper"

RSpec.describe ArticleSearchQuery, type: :model do
  describe "#articles" do
    context "検索引数がない時、全件検索できること" do
      let(:articles) { create_list(:article, 3) }
      let(:query) { ArticleSearchQuery.new }

      it { expect(query.articles).to eq articles }
    end

    context "検索引数がある時、対象の記事だけ検索できること" do
      let!(:article) { create(:article, title: "対象の件名") }
      let(:query) { ArticleSearchQuery.new(keyword: "対象") }

      before do
        create_list(:article, 2)
      end

      it { expect(query.articles).to eq [article] }
    end

    context "検索引数に日時の制定がある時、対象の記事だけ検索できること" do
      let!(:article) do
        travel_to(Time.zone.parse("2010-03-25")) { create(:article, title: "対象の件名") }
      end
      let(:query) { ArticleSearchQuery.new(from_date: "2010-3-20", to_date: "2010-4-2") }

      before do
        create_list(:article, 2)
      end

      it { expect(query.articles.count).to eq 1 }
    end
  end
end
