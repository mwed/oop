require "rails_helper"

RSpec.describe ArticleForm, type: :model do
  let(:big_category) { Category.find(1) }
  let(:middle_category) { Category.find(2) }
  let(:small_category) { Category.find(3) }

  before(:all) do
    1.upto(3) {|id| create(:category, id: id) }
  end

  after(:all) do
    Category.delete_all
  end

  describe "#big_category_label" do
    let(:article) { build_stubbed(:article) }
    let(:form) { ArticleForm.new(article) }
    it { expect(form.big_category_label).to eq big_category.name }
  end

  describe "#middle_category_label" do
    let(:article) { build_stubbed(:article) }
    let(:form) { ArticleForm.new(article) }
    it { expect(form.middle_category_label).to eq middle_category.name }
  end

  describe "#small_category_label" do
    let(:article) { build_stubbed(:article) }
    let(:form) { ArticleForm.new(article) }
    it { expect(form.small_category_label).to eq small_category.name }
  end

  describe "#category_ids" do
    let(:article) { build_stubbed(:article) }
    let(:form) { ArticleForm.new(article, attributes) }

    context do
      let(:attributes) { { big_category: "1", middle_category: "0", small_category: "0" } }
      it { expect(form.category_ids).to eq [ 1 ] }
    end

    context do
      let(:attributes) { { big_category: "0", middle_category: "1", small_category: "0" } }
      it { expect(form.category_ids).to eq [ 2 ] }
    end

    context do
      let(:attributes) { { big_category: "0", middle_category: "0", small_category: "1" } }
      it { expect(form.category_ids).to eq [ 3 ] }
    end

    context do
      let(:attributes) { { big_category: "1", middle_category: "1", small_category: "0" } }
      it { expect(form.category_ids).to eq [ 1, 2 ] }
    end

    context do
      let(:attributes) { { big_category: "1", middle_category: "0", small_category: "1" } }
      it { expect(form.category_ids).to eq [ 1, 3 ] }
    end

    context do
      let(:attributes) { { big_category: "0", middle_category: "1", small_category: "1" } }
      it { expect(form.category_ids).to eq [ 2, 3 ] }
    end

    context do
      let(:attributes) { { big_category: "1", middle_category: "1", small_category: "1" } }
      it { expect(form.category_ids).to eq [ 1, 2, 3 ] }
    end
  end

  describe "#create" do
    let(:form) { ArticleForm.new(Article.new, attributes) }

    context "valid" do
      let(:attributes) { { title: "件名件名件名", body: "本文本文本文", big_category: "1", small_category: "1" } }

      it { expect(form.create).to be_truthy }
      it { expect { form.create }.to change { Article.count }.by(1) }
      it { expect { form.create }.to change { ArticlesCategory.count }.by(2) }
    end

    context "invalid" do
      let(:attributes) { { body: "本文", big_category: "1" } }

      it { expect(form.create).to be_falsey }
    end
  end

  describe "#update" do
    let!(:article) { create(:article) }
    let(:form) { ArticleForm.new(article, attributes) }

    context "valid" do
      let(:attributes) { { title: "件名件名件名", body: "本文本文本文", big_category: "1", small_category: "1" } }

      it { expect(form.update).to be_truthy }
    end

    context "invalid" do
      let(:attributes) { { body: "本文", big_category: "1" } }

      it { expect(form.update).to be_falsey }
    end
  end
end
