require "rails_helper"

RSpec.describe ArticleSearchForm, type: :model do
  describe "#valid?" do
    context "keywordが100文字を超えている場合" do
      let(:form) { ArticleSearchForm.new(keyword: "対象" * 100) }
      it { expect(form).to be_invalid }
    end

    context "開始時のフォーマットに問題がある場合" do
      let(:form) { ArticleSearchForm.new(from_date: "2018-20-40") }
      it { expect(form).to be_invalid }
    end

    context "終了時のフォーマットに問題がある場合" do
      let(:form) { ArticleSearchForm.new(to_date: "2018-20-40") }
      it { expect(form).to be_invalid }
    end
  end

  describe "#from_date" do
    let(:form) { ArticleSearchForm.new(from_date: from_date) }

    context "日のフォーマットに問題がある場合" do
      let(:from_date) { "2018-02-29" }
      it { expect(form.from_date).to be_nil }
    end

    context "日のフォーマットに問題がない場合" do
      let(:from_date) { "2018-02-28" }
      it { expect(form.from_date).to eq Date.parse(from_date) }
    end
  end

  describe "#to_date" do
    let(:form) { ArticleSearchForm.new(to_date: to_date) }

    context "日のフォーマットに問題がある場合" do
      let(:to_date) { "2018-02-29" }
      it { expect(form.to_date).to be_nil }
    end

    context "日のフォーマットに問題がない場合" do
      let(:to_date) { "2018-02-28" }
      it { expect(form.to_date).to eq Date.parse(to_date) }
    end
  end

  describe "#articles" do
    let(:query) { instance_double(ArticleSearchQuery, articles: nil) }
    let(:form) { ArticleSearchForm.new }

    before do
      allow(ArticleSearchQuery).to receive(:new) { query }
    end

    it do
      expect(query).to receive(:articles)
      expect(form.articles).to be_nil
    end
  end
end
