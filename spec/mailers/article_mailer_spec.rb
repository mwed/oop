require "rails_helper"

RSpec.describe ArticleMailer, type: :mailer do
  describe "new_article" do
    let(:article) { build(:article) }
    let(:mail) { ArticleMailer.new_article(article) }

    it "renders the headers", :aggregation_failres do
      expect(mail.subject).to eq("New article")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body", :aggregation_failres do
      expect(mail.body.encoded).to match(article.title)
      expect(mail.body.encoded).to match(article.body)
    end
  end
end
