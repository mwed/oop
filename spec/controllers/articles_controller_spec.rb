require "rails_helper"

RSpec.describe ArticlesController, type: :controller do
  describe "GET #index" do
    before do
      create(:article)
      get :index
    end

    it "returns a success response" do
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    let!(:article) { create(:article) }

    before do
      get :show, params: { id: article.to_param }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end
  end

  describe "GET #new" do
    before do
      get :new
    end

    it "returns a success response" do
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    let!(:article) { create(:article) }

    before do
      get :edit, params: { id: article.to_param }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      let(:valid_attributes) { attributes_for(:article) }

      it "creates a new Article" do
        expect {
          post :create, params: { article_form: valid_attributes }
        }.to change { Article.count }.by(1)
      end

      it "redirects to the created article" do
        post :create, params: { article_form: valid_attributes }
        expect(response).to redirect_to(Article.last)
      end
    end

    context "with invalid params" do
      let(:invalid_attributes) { attributes_for(:article, title: nil) }

      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { article_form: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "PUT #update" do
    let!(:article) { create(:article) }

    context "with valid params" do
      let(:new_attributes) { attributes_for(:article, title: "newnewnew") }

      it "redirects to the article" do
        put :update, params: { id: article.to_param, article_form: new_attributes }
        expect(response).to redirect_to(article)
      end
    end

    context "with invalid params" do
      let(:invalid_attributes) { attributes_for(:article, title: nil) }

      it "returns a success response (i.e. to display the 'edit' template)" do
        put :update, params: { id: article.to_param, article_form: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE #destroy" do
    let!(:article) { create(:article) }

    it "destroys the requested article" do
      expect {
        delete :destroy, params: { id: article.to_param }
      }.to change { Article.count }.by(-1)
    end

    it "redirects to the articles list" do
      delete :destroy, params: { id: article.to_param }
      expect(response).to redirect_to(articles_url)
    end
  end
end
