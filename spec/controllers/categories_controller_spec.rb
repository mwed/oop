require "rails_helper"

RSpec.describe CategoriesController, type: :controller do
  describe "GET #index" do
    before do
      create(:category)
      get :index
    end

    it "returns a success response" do
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    let!(:category) { create(:category) }

    before do
      get :show, params: { id: category.to_param }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end
  end

  describe "GET #new" do
    before do
      get :new
    end

    it "returns a success response" do
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    let!(:category) { create(:category) }

    before do
      get :edit, params: { id: category.to_param }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      let(:valid_attributes) { attributes_for(:category) }

      it "creates a new Category" do
        expect {
          post :create, params: { category: valid_attributes }
        }.to change { Category.count }.by(1)
      end

      it "redirects to the created category" do
        post :create, params: { category: valid_attributes }
        expect(response).to redirect_to(Category.last)
      end
    end

    context "with invalid params" do
      let(:invalid_attributes) { attributes_for(:category, name: nil) }

      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { category: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) { attributes_for(:category, name: "New") }
      let!(:category) { create(:category) }

      before do
        put :update, params: { id: category.to_param, category: new_attributes }
      end

      it "redirects to the category" do
        expect(response).to redirect_to(category)
      end
    end

    context "with invalid params" do
      let(:invalid_attributes) { attributes_for(:category, name: nil) }
      let!(:category) { create(:category) }

      before do
        put :update, params: { id: category.to_param, category: invalid_attributes }
      end

      it "returns a success response (i.e. to display the 'edit' template)" do
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE #destroy" do
    let!(:category) { create(:category) }

    it "destroys the requested category" do
      expect {
        delete :destroy, params: { id: category.to_param }
      }.to change { Category.count }.by(-1)
    end

    it "redirects to the categories list" do
      delete :destroy, params: { id: category.to_param }
      expect(response).to redirect_to(categories_url)
    end
  end
end
